import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        themes : {
            light: {
                background: '#ffffff',
                backgroundDark: '#f5f4f6',
                primary: '#3D4351',
                secondary: '#bd9170',
                tertiary: '#7A8387',
                accent: '#1a237e',
                error: '#FF5252',
                info: '#2196F3',
                success: '#4CAF50',
                warning: '#FFC107',
            },
            dark: {
                background: '#7A8387',
                backgroundDark: '#5c6367',
                primary: '#ffffff',
                secondary: '#f9c297',
                tertiary: '#7A8387',
                accent: '#82B1FF',
                error: '#FF5252',
                info: '#1f7acc',
                success: '#4CAF50',
                warning: '#cd9705',
            }
        },
        dark: false
    }
});
