import Vue from 'vue'
import VueRouter from 'vue-router'
import Index from "../Vues/Index";

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Index,
        meta: {onlyNotLoggedIn: true}
    },
    {
        path: "*",
        name: "error",
        redirect: "/",
    },
];

const index = new VueRouter({
    mode: 'history',
    routes
});

index.beforeEach((to, from, next) => {

    next();
});

export default index
